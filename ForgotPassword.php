<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="CSS/Layout.css" rel="stylesheet" type="text/css" />
<link href="CSS/Menu.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Forgot Password</title>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>
<div class="Holder">
<div class="Header">Inventory Model Software Package</div>
<div class="NavBar">
	<nav>
    	<ul>
        	<li><a href="index.php">Login</a></li>
            <li><a href="Register.php">Register</a></li>

        </ul>
    </nav>
</div>
<div class="Content">
	<div class="PageHeading">
	  <h1>Email Password</h1>
	</div>
	<div class="ContentLeft">
	  <h2>EMPW Message</h2>
	  <h6><br />
	    Your Message<br />
	  </h6>
	  
	</div>
    <div class="ContentRight">
      <form id="EMPWForm" name="EMPWForm" method="post" action="EmailPasswordScript.php">
        <span id="sprytextfield1">
        <label for="Email"></label>
        <input name="Email" type="text" class="StyleText" id="Email" />
        <br />
        <br />
        <span class="textfieldRequiredMsg">A value is required.</span></span>
        <input type="submit" name="EMPWButton" id="EMPWButton" value="Email Password" />
      </form>
    </div>
</div>
<div class="Footer">&copy;Your Name <a href="index.php">Admin</a></div>
</div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
</script>
</body>
</html>